use std::sync::Mutex;

use crate::path::Path;

lazy_static! {
    /// global data structure to collect solutions as they are found
    static ref SOLUTIONS: Mutex<Vec<Path>> = Mutex::new(Vec::new());
}

pub fn add(path: &Path) {
    SOLUTIONS.lock().unwrap().push(path.clone());
}

pub fn get() -> Vec<Path> {
    SOLUTIONS.lock().unwrap().clone()
}
