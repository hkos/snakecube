use std::io::Write;
use std::sync::atomic::{AtomicUsize, Ordering};

const COUNT_PLACEMENTS: bool = false;
static PLACEMENTS: AtomicUsize = AtomicUsize::new(0);

#[inline(always)]
pub fn get() -> usize {
    PLACEMENTS.load(Ordering::Relaxed)
}

#[inline(always)]
pub fn count() {
    if COUNT_PLACEMENTS {
        PLACEMENTS.fetch_add(1, Ordering::AcqRel);
    }
}

#[inline(always)]
pub fn print() {
    if COUNT_PLACEMENTS { print!("(#{})", get()) } else { print!(".") };
    let _ = std::io::stdout().flush();
}
