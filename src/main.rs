use snakecube::walker::Walker;

// const SNAKE3: &[u8] = &[2, 1, 1, 2, 1, 2, 1, 1, 2, 2, 1, 1, 1, 2, 2, 2, 2];
const SNAKE4: &[u8] = &[2, 1, 2, 1, 1, 3, 1, 2, 1, 2, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 2, 3, 1, 1, 1, 3, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1];

fn main() {
    let solutions = Walker::<4>::search(SNAKE4);
    // let solutions = Walker::<3>::search(SNAKE3);

    solutions.iter().enumerate().for_each(|(num, s)| {
        println!("Solution #{}: {:?}", num, s);
    });
}