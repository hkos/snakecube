use rayon::prelude::*;

use crate::space::{Direction, Space};
use crate::solutions;
use crate::statistics;
use crate::path::Path;


// fix the first two moves as Directions::BACK and Directions::RIGHT
// (this reduces the search-space and clearly only removes rotated/mirrored solutions)
const FIXED_MOVES: &[Direction] = &[Direction::BACK, Direction::RIGHT];


// process this many levels of recursion threaded, the rest of the depth unthreaded.
// a value of "0" implies no parallelism.
const DEPTH_THREADED: usize = 8;


#[derive(Copy, Clone, Default)]
pub struct Walker<const S: usize> {
    space: Space<S>,
    path: Path,
}

impl <const S: usize> Walker<S> {
    pub fn search(segments: &[u8]) -> Vec<Path> {
        let mut w = Self::default();

        w.place_fixed(&segments[0..FIXED_MOVES.len()]);
        w.recurse(&segments[FIXED_MOVES.len()..]);

        println!("\nsearch done. total # of placements: {}\n", statistics::get());

        solutions::get()
    }

    fn place_fixed(&mut self, segments: &[u8]) {
        for &dir in FIXED_MOVES {
            let next_segment_len = segments[self.path.len()];

            assert!(self.space.check_segment(next_segment_len, dir));
            self.place(next_segment_len, dir);
        }
    }

    #[inline(always)]
    fn recurse(&self, segments: &[u8]) {
        match segments.split_first() {
            None => {
                solutions::add(&self.path);
                statistics::print();
            }
            Some((&current_size, remaining_segments)) => {
                // check all followup directions, recurse if check_segment() is true
                let next_dirs = self.path.last().next_directions();

                let is_placeable =
                    |dir: &&Direction| self.space.check_segment(current_size, **dir);

                let do_place = |dir: &Direction| {
                    let mut placed = *self; // clone
                    placed.place(current_size, *dir);
                    placed.recurse(remaining_segments);
                };

                // iter() in parallel or serially depending on recursion depth
                if self.path.len() < DEPTH_THREADED + FIXED_MOVES.len() {
                    next_dirs.par_iter().filter(is_placeable).for_each(do_place)
                } else {
                    next_dirs.iter().filter(is_placeable).for_each(do_place)
                }
            }
        }
    }

    #[inline(always)]
    fn place(&mut self, size: u8, dir: Direction) {
        statistics::count();

        self.space.place_segment(size, dir);
        self.path.push(dir);
    }
}
