#[macro_use]
extern crate lazy_static;

pub mod path;
pub mod space;
pub mod walker;
pub mod solutions;
pub mod statistics;