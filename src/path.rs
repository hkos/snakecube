use std::fmt::{Debug, Formatter, Result};

use crate::space::Direction;

// FIXME: fixed size constant?!
const MAX_PATH_LEN: usize = 46;

#[derive(Clone, Copy)]
pub struct Path {
    len: usize,
    elements: [Option<Direction>; MAX_PATH_LEN],
}

impl Default for Path {
    fn default() -> Self {
        Self { len: 0, elements: [None; MAX_PATH_LEN] }
    }
}

impl Debug for Path {
    fn fmt(&self, formatter: &mut Formatter) -> Result {
        let solution = self.elements[..self.len()].iter()
            .map(|&dir| format!("{:?}", dir.unwrap()))
            .map(|d| d.split_at(1).0.to_owned())
            .collect::<Vec<_>>().join("");

        formatter.write_fmt(format_args!("{}", solution))
    }
}

impl Path {
    #[inline(always)]
    pub fn push(&mut self, dir: Direction) {
        self.elements[self.len] = Some(dir);
        self.len += 1;
    }

    #[inline(always)]
    pub fn last(&self) -> Direction {
        self.elements[self.len - 1].unwrap()
    }

    #[inline(always)]
    pub const fn len(&self) -> usize { self.len }
}
