use std::cmp::{min, max};

#[derive(Clone, Copy, Debug)]
pub enum Direction { UP, DOWN, LEFT, RIGHT, FRONT, BACK }

impl Direction {
    const UD: [Self; 4] = [Self::FRONT, Self::RIGHT, Self::LEFT, Self::BACK];
    const RL: [Self; 4] = [Self::UP, Self::FRONT, Self::BACK, Self::DOWN];
    const BF: [Self; 4] = [Self::UP, Self::RIGHT, Self::LEFT, Self::DOWN];

    #[inline(always)]
    pub fn next_directions(self) -> [Self; 4] {
        match self {
            Self::UP | Self::DOWN => Self::UD,
            Self::RIGHT | Self::LEFT => Self::RL,
            Self::BACK | Self::FRONT => Self::BF
        }
    }
}


#[derive(Clone, Copy, Debug)]
pub struct Point { d: [u8; 3] }

impl Point {
    #[inline(always)]
    const fn new(x: u8, y: u8, z: u8) -> Self {
        Self { d: [x, y, z] }
    }

    // not setting #[inline] here seems to lead to ~5% faster parallel code,
    // but ~5% slower single threaded code. (why?!)
    // #[inline(always)]
    fn move_step(&mut self, dir: Direction) {
        match dir {
            Direction::LEFT => self.d[0] += 1,
            Direction::RIGHT => self.d[0] -= 1,
            Direction::FRONT => self.d[1] += 1,
            Direction::BACK => self.d[1] -= 1,
            Direction::UP => self.d[2] += 1,
            Direction::DOWN => self.d[2] -= 1,
        }
    }
}


/// we only ever want to use a range of SIZE different coordinate values per
/// dimension, so we only allocate that much space, and then wrap incoming
/// coordinates "modulo SIZE".
///
/// expected input coordinate values are 1 -> SIZE -> SIZE*2-1,
/// "SIZE" is the origin of the coordinate system
#[derive(Clone, Copy)]
struct Cube<const S: usize> {
    points: [[[bool; S]; S]; S],
    min: Point,
    max: Point,
}

impl<const S: usize> Cube<S> {
    /// returns:
    /// - a Cube of points
    /// - a starting Point (which has been pre-set to "used")
    #[inline(always)]
    fn new() -> (Self, Point) {
        let mut cube = Self {
            points: [[[false; S]; S]; S],
            min: Self::origin(),
            max: Self::origin(),
        };

        cube.set(&Self::origin());

        (cube, Self::origin())
    }

    #[inline(always)]
    const fn origin() -> Point { Point::new(S as u8, S as u8, S as u8) }

    #[inline(always)]
    const fn wrap(c: u8) -> usize { c  as usize % S }

    #[inline(always)]
    const fn wrapped(p: &Point) -> (usize, usize, usize) {
        (Self::wrap(p.d[0]), Self::wrap(p.d[1]), Self::wrap(p.d[2]))
    }

    #[inline(always)]
    const fn get(&self, p: &Point) -> bool {
        let (x, y, z) = Self::wrapped(p);
        self.points[x][y][z]
    }

    #[inline(always)]
    fn set(&mut self, p: &Point) {
        debug_assert!(self.is_in_bounds(p));

        let (x, y, z) = Self::wrapped(p);
        self.points[x][y][z] = true;
    }

    #[inline(always)]
    fn is_in_bounds(&self, p: &Point) -> bool {
        // are all coords of the new point in the allowed range?
        (0..3).all(|dim| {
            p.d[dim] > 0
                && p.d[dim] < 2 * S as u8
                && max(self.max.d[dim], p.d[dim]) - min(self.min.d[dim], p.d[dim]) < S as u8
        })
    }

    #[inline(always)]
    fn update_bounds(&mut self, p: Point) {
        // update bounds of used space
        for dim in 0..3 {
            if p.d[dim] < self.min.d[dim] {
                self.min.d[dim] = p.d[dim];
            } else if p.d[dim] > self.max.d[dim] {
                self.max.d[dim] = p.d[dim];
            }
        }
    }
}


#[derive(Clone, Copy)]
pub struct Space <const S: usize> { cube: Cube<S>, pos: Point }

impl<const S:usize> Default for Space<S>{
    #[inline(always)]
    fn default() -> Self {
        let (cube, pos) = Cube::new();
        Self { cube, pos }
    }
}

impl <const S: usize> Space <S> {
    #[inline(always)]
    fn is_possible(&self, p: &Point) -> bool {
        self.cube.is_in_bounds(p) && !self.cube.get(p)
    }

    /// check if placement is possible before cloning Space, Solution and Path
    /// (approx 75% of all placements in the search tree fail this test)
    #[inline(always)]
    pub fn check_segment(&self, size: u8, dir: Direction) -> bool {
        let mut pos = self.pos;

        (0..size).all(|_| {
            pos.move_step(dir);
            self.is_possible(&pos)
        })
    }

    /// if check_segment() has been run before, placement is guaranteed to
    /// work, so we don't do any checking here
    #[inline(always)]
    pub fn place_segment(&mut self, size: u8, dir: Direction) {
        // set all points of this segment as used
        for _ in 0..size {
            self.pos.move_step(dir);
            debug_assert!(self.is_possible(&self.pos));
            self.cube.set(&self.pos);
        }

        self.cube.update_bounds(self.pos);
    }
}