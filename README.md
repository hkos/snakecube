**SnakeCube solver**

An implementation of a brute-force solver for
https://en.wikipedia.org/wiki/Snake_cube puzzles.

The code is a port of https://github.com/AbcAeffchen/SnakeCubeSolver
(and was written to familiarize myself with Rust).

Run the code using
`cargo run --release`